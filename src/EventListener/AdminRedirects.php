<?php

namespace ContextualCode\EzPlatformAdminURLAliasRedirectsBundle\EventListener;

use Exception;
use eZ\Publish\API\Repository\LocationService;
use eZ\Publish\API\Repository\URLAliasService;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use eZ\Publish\Core\MVC\Symfony\SiteAccess;
use EzSystems\EzPlatformAdminUiBundle\EzPlatformAdminUiBundle;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class AdminRedirects
{
    /** @var array */
    protected $siteAccessGroups;

    /** @var URLAliasService */
    protected $urlAliasService;

    /** @var ConfigResolverInterface */
    protected $configResolver;

    /** @var LocationService */
    protected $locationService;

    /** @var array */
    static $siteAccessNameToRootLocationId = [];

    /** @var array  */
    static $siteAccessNameToURLAliasPath = [];

    /**
     * AdminRedirects constructor.
     * @param array $siteAccessGroups
     * @param URLAliasService $urlAliasService
     * @param ConfigResolverInterface $configResolver
     * @param LocationService $locationService
     */
    public function __construct(
        array $siteAccessGroups,
        URLAliasService $urlAliasService,
        ConfigResolverInterface $configResolver,
        LocationService $locationService
    ) {
        $this->siteAccessGroups = $siteAccessGroups;
        $this->urlAliasService = $urlAliasService;
        $this->configResolver = $configResolver;
        $this->locationService = $locationService;
    }

    /**
     * @param ExceptionEvent $event
     * @throws Exception
     */
    public function onKernelException(ExceptionEvent $event)
    {
        if (!$this->isAdminException($event)) {
            return;
        }

        $exception = $event->getThrowable();
        if ($exception instanceof HttpExceptionInterface === false) {
            return;
        }

        if ($exception->getStatusCode() !== 404) {
            return;
        }

        if ($event->getResponse() instanceof RedirectResponse) {
            return;
        }

        $url = $event->getRequest()->getRequestUri();
        if (empty(self::$siteAccessNameToRootLocationId)) {
            foreach ($this->siteAccessGroups as $siteAccessGroupKey => $siteAccessGroup) {
                if ($siteAccessGroupKey === EzPlatformAdminUiBundle::ADMIN_GROUP_NAME) {
                    continue;
                }
                foreach ($siteAccessGroup as $siteAccessName) {
                    self::$siteAccessNameToRootLocationId[$siteAccessName] = $this->configResolver->getParameter(
                        'content.tree_root.location_id',
                        'ezsettings',
                        $siteAccessName
                    );
                }
            }
            foreach (self::$siteAccessNameToRootLocationId as $siteAccessName =>  $rootLocationId) {
                try {
                    $location = $this->locationService->loadLocation($rootLocationId);
                    $URLAlias = $this->urlAliasService->reverseLookup($location);
                    self::$siteAccessNameToURLAliasPath[$siteAccessName] = $URLAlias->path;
                } catch (Exception $e) {
                    continue;
                }
                if (
                    !$this->isAdminSiteAccessName($siteAccessName) &&
                    strpos($url, self::$siteAccessNameToURLAliasPath[$siteAccessName]) === 0
                ) {
                    return;
                }
            }
        }

        foreach (self::$siteAccessNameToURLAliasPath as $URLAliasPath) {
            try {
                $URLAlias = $this->urlAliasService->lookup($URLAliasPath . $url);
                $response = new RedirectResponse($URLAlias->path);
                $event->setResponse($response);
                return;
            } catch (Exception $e) {
                continue;
            }
        }
    }

    /**
     * @param ExceptionEvent $event
     * @return bool
     */
    protected function isAdminException(ExceptionEvent $event): bool
    {
        $request = $event->getRequest();

        /** @var SiteAccess $siteAccess */
        $siteAccess = $request->get('siteaccess', null);

        return $this->isAdminSiteAccessName($siteAccess->name ?? '');
    }

    /**
     * @param string $siteAccessName
     * @return bool
     */
    protected function isAdminSiteAccessName(string $siteAccessName): bool
    {
        return \in_array(
            $siteAccessName,
            $this->siteAccessGroups[EzPlatformAdminUiBundle::ADMIN_GROUP_NAME]
        );
    }
}
